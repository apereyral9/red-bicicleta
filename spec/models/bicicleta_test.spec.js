var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');


describe('Testing Bicicletas', function(){
    // beforeAll((done) => { mongoose.connection.close(done) });


    
   beforeEach(function(done){
    mongoose.connect('mongodb://localhost/testdb', {useUnifiedTopology: true, useNewUrlParser: true});
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
       console.log('We are connected to test database!');
       done();
    });
   });

   afterEach(function(done){
       Bicicleta.deleteMany({},function(err,success){
           if (err) console.log(err);
           done();
       });
   });

   describe('Bicicleta.createInstance',() => {
       it('crea una instancia de Bicicleta', () => {
           var bici = Bicicleta.createInstance(1,"verde","urbana",[-34.5,-54.1]);
           expect(bici.code).toBe(1);
           expect(bici.color).toBe("verde");
           expect(bici.modelo).toBe("urbana");
           expect(bici.ubicacion[0]).toEqual(-34.5);
           expect(bici.ubicacion[1]).toEqual(-54.1);
       })
   });

   describe('Bicicleta.allBicis',() => {
       it('comienza vacia',(done) => {
           Bicicleta.allBicis(function(err,bici){
               expect(bici.length).toBe(0);
               done();
           });
       });
   });

   describe('Bicicleta.add', () => {
       it('agrega solo una bici', (done) => {
           var aBici =  new Bicicleta({code:1,color:"verde",modelo:"urbana"});
           Bicicleta.add(aBici,function(err, newBici){
               if(err) console.log(err);
               Bicicleta.allBicis(function(err,bici){
                   expect(bici.length).toEqual(1);
                   expect(bici[0].code).toEqual(aBici.code);

                   done();
               });
           });
       });
   });

   describe('Bicicleta.finfByOne', () => {
       it('debe devolver la bici con node 1',(done) =>{
           Bicicleta.allBicis(function(err,bici){
               expect(bici.length).toBe(0);

               var aBici = new Bicicleta({code: 1, color:"verde", modelo:"urbana"});
               Bicicleta.add(aBici, function(err, newBici){
                   if(err) console.log(err);


                   var aBici2 = new Bicicleta({code: 2, color: "roja", modelo:"urbana"});
                   Bicicleta.add(aBici2,function(err,newBici){
                       if (err) console.log(err);
                       Bicicleta.findByCode(1,function (error,targetBici){
                        expect(targetBici.code).toBe(aBici.code);
                        expect(targetBici.color).toBe(bici.color);
                        expect(targetBici.modelo).toBe(aBici.modelo);
 
                        done();
                      });
                   });
               });
           });
       });
   });

});