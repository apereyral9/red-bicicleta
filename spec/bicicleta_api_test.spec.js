var Bicicleta = require('../models/bicicleta');
var request = require('request');
var server = require('../bin/www');


describe('Bicicleta API', () => {
  describe('GET Bicicleta /', () => {
    it('Status 200', () => {
         expect(Bicicleta.allBicis.length).toBe(0);

         var a = new Bicicleta(1, 'rojo','urbana',[-34.9064363,-56.1387044]);
         Bicicleta.add(a);

         request.get('http://localhost:3000/api/bicicletas', function(error,response,body){
         expect(response.statusCode).toBe(200);    
      });
    });
  });
});
beforeEach(() => { console.log('testeando...')  });

describe('POST Bicicleta /create', () => {
      it('Status 200', (done) => {
          var headers = {'content-type' : 'application/json'};
          var aBici = '{"id":10,"color":"rojo","modelo":"urbana","lat":-34,"lng":-54}';
          request.post({
              headers:headers,
              url: 'http://localhost:3000/api/bicicletas/create',
              body: aBici
          }, function(error,responde,body){
              expect(responde.statusCode).toBe(200);
              expect(Bicicleta.findByID(10).color).toBe("rojo");
              done();
        });
      });
    });
  